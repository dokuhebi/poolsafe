---
title: 'Donate'
---

The mission of PoolSafe needs your help and your money to continue our mission. In order to counter the work being done by the large pool industries, we need the support
of people like you, regular folks who want to help make a difference.
