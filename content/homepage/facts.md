---
title: "Pool Violence Facts"
weight: 2
header_menu: true
---

![Head in Hands](images/head-in-hands.jpg)


Here are the **facts** on swimming pool dangers in the United States, as reported by the Federal CDC:

Every day, about ten people die from unintentional drowning. Of these, two are children aged 14 or younger. Drowning ranks fifth among the leading causes of unintentional injury death in the United States.  All of these deaths are preventable. 

From 2005-2014, there were an average of 3,536 fatal unintentional drownings (non-boating related) annually in the United States — about ten deaths per day. An additional 332 people died each year from drowning in boating-related incidents.

About one in five people who die from drowning are children 14 and younger.
