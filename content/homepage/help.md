---
title: "How You Can Help"
weight: 4
header_menu: true
---

![Kids in Sprinkler](images/kids-in-sprinkler.jpg)

PoolSafe promotes a number of efforts you can help support to reduce the incidents of pool violence.

1. Contact your local legislators to find out what efforts they promote to reduce pool violence.
2. Support legislation to restrict personal ownership of swimming pools by people without training in pool safety.
3. Promote safe alternatives to swimming pools, like lawn sprinklers.
4. Work to remove the CDC swimming "safety" [guidelines](https://www.cdc.gov/homeandrecreationalsafety/water-safety/waterinjuries-factsheet.html#tabs-1-3) influenced by the pool promotion lobby.
5. If you are a pool owner, consider replacing your pool with a [safer alternative](https://www.amazon.com/gp/product/B01AD1TZ9M/).
