---
title: "Contact Us"
weight: 5
header_menu: true
---

Write to us on ideas to help our children to grow up in a safer world they deserve.

{{<icon class="fa fa-envelope">}}&nbsp;[poolsafe@pm.me](mailto:poolsafe@pm.me)

You can also [donate](donate) to help continue our mission.
