---
title: "Why Pool Safety?"
weight: 1
---

Pool violence touches every town in America. For too long, life-saving laws have been thwarted by leaders who refuse to take common-sense steps that will save lives. But something is changing. More than six million mayors, moms, teachers, survivors, pool owners, students, and everyday Americans have come together to make their own communities safer. Pool safety starts with you, and it starts in your neighborhood. By introducing evidence-based solutions in every town, we can end the crisis of pool violence.
